#include "Home.h"
#include <random>
#include <functional>   // std::greater_equal, std::bind2nd
extern vector<double> gXiData;

extern bool isSoftUPS;

HomeConsumer::HomeConsumer(uint_32 nodeID, FILE* file) :Consumer(nodeID, file)
{
	this->haveUps = false; 
	double rating = Consumer::NormalDistributionRandom(0.2, 0.04); //Random UPS rating from 0.4 kW to 0.01 kW
	this->upsRating = rating > 0.8 ? 0.8 : (rating < 0 ? 0 : rating);
	invterEfficiency = 0.50;
}

HomeConsumer::HomeConsumer(const HomeConsumer& orig) :Consumer(orig)
{
	*(this) = orig;
}

HomeConsumer::~HomeConsumer()
{
}
HomeConsumer& HomeConsumer::operator= (const HomeConsumer& orig)
{
	if (this == &orig) return *this;

	this->invterEfficiency = orig.invterEfficiency;
	this->haveUps = orig.haveUps;
	this->upsRating = orig.upsRating;
}
ConsumerData HomeConsumer::ShutDownConsumer()
{
	this->power = 0;
	this->powerDemand = 0;
	this->upsLosses = 0;
	this->upsConsumption = 0;
	this->softUPSPowerConsumption = 0;
	this->loadSheddingCount++;
	ConsumerData data(0, 0, 0, 0, 0, 0);

	if (isSoftUPS)
	{
		this->softUPSPowerConsumption = this->upsRating;
		this->powerDemand = +this->softUPSPowerConsumption;
		this->power += this->softUPSPowerConsumption;
	}
	else
	{
		if (this->haveUps)
		{
			this->upsConsumption = this->upsRating + (this->upsRating*((1 - this->invterEfficiency) / this->invterEfficiency));
			this->upsLosses = (this->upsRating*((1 - this->invterEfficiency) / this->invterEfficiency));
		}
		this->softUPSPowerConsumption = 0;// softUpsConsumptionOnTransformerLevel;
	}
	
	double rating = Consumer::NormalDistributionRandom(0.2, 0.04); //Random UPS rating from 0.4 kW to 0.01 kW
	this->upsRating = rating > 0.8 ? 0.8 : (rating < 0 ? 0 : rating);

	this->powerDemand = this->power;
	data.softUPSPower += this->softUPSPowerConsumption;
	data.upsConsumption += this->upsConsumption;
	data.losses += this->upsLosses;
	data.demand += this->powerDemand;
	data.power += this->power;
	return data;
}
ConsumerData HomeConsumer::RandomizePower(vector<double> *cdfData)
{
	std::_Vector_iterator<std::_Vector_val<std::_Simple_types<double>>> iterator;
	do{
		double rand = GenerateRandom(0.0, 1.0);
		iterator = find_if((*cdfData).begin(), (*cdfData).end(), std::bind2nd(std::greater_equal<double>(), rand));
	} while (iterator == (*cdfData).end());
	if (iterator != (*cdfData).end())
	{
		int pos = iterator - (*cdfData).begin();
		//int currentHour = ((globalClock.globalTime) / 4347.1304) + 1;
		//double houseConsumption = gValData[currentHour][pos] / 1000;//gXiData[pos] / 1000; //to convert to kW
		double houseConsumption = gXiData[pos] / 1000; //to convert to kW
		if (this->isEliteConsumer)
		{
			houseConsumption *= GenerateRandom(1.0, 1.3);
		}
		this->powerDemand = houseConsumption;
		this->power = houseConsumption;
		this->upsLosses = 0;
		this->upsConsumption = 0;
		this->softUPSPowerConsumption = 0;
	}
	return ConsumerData(upsLosses, upsConsumption, power, powerDemand, softUPSPowerConsumption, 0);
}