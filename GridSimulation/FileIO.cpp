#include "FileIO.h"
#include <vector>
#include <fstream>
#include <iterator>
FileIO::FileIO(void)
{
}


FileIO::~FileIO(void)
{
}
std::vector<double> FileIO::ReadFile(std::string fileName)
{
	//courtesy: http://stackoverflow.com/questions/6755111/read-input-files-fastest-way-possible
	//http://stackoverflow.com/questions/8365013/reading-line-from-text-file-and-putting-the-strings-into-a-vector
	std::vector<double> vec;
	std::ifstream is(fileName);
	copy(std::istream_iterator<double>(is),
         std::istream_iterator<double>(),
         back_inserter(vec));
	return vec;
}
std::vector<double> FileIO::ReadCdfData(int hour)
{
	std::string fileName=DiretoryPath+"/fr"+std::to_string(hour)+".txt";
	return ReadFile(fileName);
}
std::vector<double> FileIO::ReadXiData()
{
	std::string fileName=DiretoryPath+"/xi.txt";
	return ReadFile(fileName);
}
std::vector<double> FileIO::ReadValData(int hour)
{
	std::string fileName = DiretoryPath + "/xi"+ std::to_string(hour) + ".txt";
	return ReadFile(fileName);
}