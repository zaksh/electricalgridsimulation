#pragma once
#include "Consumer.h"
class HomeConsumer :	public Consumer
{
protected:
	virtual ConsumerData ShutDownConsumer();
	virtual ConsumerData RandomizePower(vector<double> *cdfData);
public:
	bool haveUps;
	double invterEfficiency;
	float upsRating;// = 0.9;//1;
	HomeConsumer(uint_32 nodeID, FILE* file);
	HomeConsumer(const HomeConsumer& orig);
	HomeConsumer& operator= (const HomeConsumer& orig);
	~HomeConsumer();
};

