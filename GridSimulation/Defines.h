#ifndef _DEFINES_H_
#define _DEFINES_H_

#define uint_32 unsigned int 
#define uint_64 unsigned long
#define uint_16 unsigned short
#define uint_8   unsigned char

#define   TIMETICK_TO_USEC	100///how much is microsec equal 1 time tick (atomic unit of our simulator)
#define   SEC_TO_TIMETICK      ((double)1000000/TIMETICK_TO_USEC)//how many time ticks equal 1 second

#define DEBUG_LEVEL_CON		5 //higher debug values are of less importance
#define DEBUG_LEVEL_FILE	3 //2 zaksh changed it //higher debug values are of less importance
#define OUTFILE(fptr,id,eventStr,detailStr,dbg) if( dbg<= DEBUG_LEVEL_FILE) fprintf(fptr,"%ld\t %d\t %s \t %s\n",globalClock.globalTime,id,eventStr,detailStr);
#define OUTCON(id,eventStr,detailStr,dbg) if (dbg<= DEBUG_LEVEL_CON) printf("%ld\t %d\t %s \t %s\n",globalClock.globalTime,id,eventStr,detailStr);


#define	min(a,b)	((a) < (b) ? (a) : (b))
#define	max(a,b)	((a) > (b) ? (a) : (b))

#include "timers.h"

enum eventType {et_idle, et_generation, et_consumption, et_BalanceSupplyDemandGap, et_reduceConsumption,et_reduceGeneration,et_increaseGeneration};
enum NodeType {nt_dummy,nt_Genco, nt_Disco, nt_CentralBody, nt_ZoneDistribution, nt_GridStation, nt_Feederes, nt_Transformer};
enum LoadSheddingType{lt_HighLoadFirst,lt_Random,lt_Fair};

/////
struct EventInfo {
	short nodeId_; //the id of the node for which this event occured, 
	double powerVariation;
	eventType eType_; /// event Type... Generation, consumption, increase generation,reduce generation/consumption.
	timeTicks timeLeft_;//assigned with the time left till the event expiry	
};
///
class Event: public TimerCallback{
	private:
		EventInfo eInfo_;	

	public:
		Event(EventInfo eInfo) : eInfo_(eInfo){};
		~Event(){};
		int Expire();
		

};

class ConsumerData
{
public:
	double losses;
	double upsConsumption;
	double power;
	double softUPSPower;
	int consumersCount;
	double demand;
	ConsumerData()
	{
		losses = 0;
		power = 0;
		softUPSPower = 0;
		consumersCount = 0;
		demand = 0;
		upsConsumption = 0;
	}
	ConsumerData(double Losses, double UpsConsumption, double Power, double PowerDemand, double SoftUPSPower, int NoOfConsumers)
	{
		losses = Losses;
		power = Power;
		softUPSPower = SoftUPSPower;
		consumersCount = NoOfConsumers;
		demand = PowerDemand;
		upsConsumption = UpsConsumption;
	}
};
class Output
{
public:
	int numberOfFeedersTurnedOff;
	double actualPowerDemand;
	double powerSupplied;
	double upsLosses;
	double upsConsumption;
	double softUPSConsumption;
	Output()
	{
		numberOfFeedersTurnedOff = 0;
		actualPowerDemand = 0;
		powerSupplied = 0;
		upsLosses = 0;
		upsConsumption = 0;
		softUPSConsumption = 0;
	}
};

////
#endif
