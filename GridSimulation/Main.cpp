#include "Simulator.h"
#include "Globals.h"
#include <ctime>
void main(int argc, char *argv[])
{
	//isSoftUPS = true;
	string ioDirectory ="./";
	FileIO file;
	LoadConfigration();
	//isSoftUPS = true;
	if (isSoftUPS)
	{
		OUTCON(0, "MAIN", "\t...:: SOFTUPS Scenario ::....", 1);
	}
	else
	{
		OUTCON(0, "MAIN", "\t...:: Conventional UPS Scenario ::....", 1);
	}
	OUTCON(0,"MAIN","Reading xi Data",3);
	/*for (int i = 1; i <= 2; i++)
	{
		gValData[i] = file.ReadValData(i);
	}*/
	gXiData=file.ReadXiData();
	sort(gXiData.begin(),gXiData.end());
	FILE *stream;
	string message;
	string rand = to_string(std::clock());
	string outFileName = ioDirectory + "trace" + rand+ ".out";
	message="Initializing Simulator";
	OUTCON(0,"MAIN",message.c_str(),3);
	for(int i=1; i<=2; i++)
	{
		gCdfMap[i]=file.ReadCdfData(i);
	}
	if(fopen_s( &stream, outFileName.c_str(), "w+" )==0)
	{
		Simulator simulator(stream,0.10);
		//Simulator simulator(fopen(outFileName.c_str(),"w+"),10);
		//simulator.fptr=fopen(outFileName.c_str(),"w+");
		//OUTFILE(simulator.fptr,"-1","Test","Writing test",3);
		int simTimeinSec=10;	
		simulator.MAX_SIMTIME =simTimeinSec*SEC_TO_TIMETICK;//run Simulator for simTimeinSec
		simulator.SimStart();
		_fcloseall();
	}
	else
	{
		message="File opening error";
		OUTCON(0,"MAIN",message.c_str(),1);
	}
	FILE *streamFeeders;
	string outFeederFileName = ioDirectory + "feederTrace" + rand + ".out";
	if (fopen_s(&streamFeeders, outFeederFileName.c_str(), "w+") == 0)
	{
		for (auto iterator = gFeedersTurnedOff.begin(); iterator < gFeedersTurnedOff.end(); iterator++)
		{
			fprintf(streamFeeders, "%d\n", (*iterator));
		}
		_fcloseall();
	}
	FILE *streamOutPut;
	string outputDataFileName = ioDirectory + "OutPut" + rand + ".out";
	if (fopen_s(&streamOutPut, outputDataFileName.c_str(), "w+") == 0)
	{
		for (auto iterator = gOutputData.begin(); iterator < gOutputData.end(); iterator++)
		{
			string line = to_string((*iterator).actualPowerDemand) + "," + to_string((*iterator).upsLosses) + "," + 
				to_string((*iterator).upsConsumption) + "," + to_string((*iterator).softUPSConsumption)
				+ "," + to_string((*iterator).powerSupplied) + "," + to_string((*iterator).numberOfFeedersTurnedOff);
			fprintf(streamOutPut, "%s\n", line.c_str());
		}
		_fcloseall();
	}

}
