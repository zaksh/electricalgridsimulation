#include "Consumer.h"
#include "Home.h"
#include <random>
#include <map>
extern	Timers gTimerManager;
extern cVTime globalClock;///Our global clock!
//extern vector<Node*> gNodeVector;///vector that will keep track of all the nodes in the Simulator
extern Timers gTimerManager;
//extern vector<double> gXiData;
extern std::map<int, vector<double>> gCdfMap;
//extern std::map<int, vector<double>> gValData;
extern double gTotalUpsLosses;
extern double gTotalSoftUpsConsumption;
extern double gSaving;
extern vector<uint_32> gFeedersTurnedOff;
extern bool isSoftUPS;
extern LoadSheddingType loadSheddingType;
extern vector<Output> gOutputData;

Consumer::Consumer(uint_32 nodeID, FILE* file) :Node(nodeID, file)
{
	//http://stackoverflow.com/questions/8440213/how-to-define-a-const-double-inside-a-classs-header-file
	inverterPenetration = 0.6; //0-1 ==> 0-100%
	softUPSPowerConsumption = 0;
	consumersCount = 0;
	powerDemand = 0;
	upsLosses = 0;
	upsConsumption = 0;
	loadSheddingCount = 0;
	isEliteConsumer = false;
}
Consumer::Consumer(const Consumer& orig) :Node(orig)
{
	*(this) = orig;
}
Consumer& Consumer::operator= (const Consumer& orig){
	if (this == &orig) return *this;


	myId = orig.myId;// the unique node id kept for purposes of identification
	this->power = orig.power;
	this->nodeType = orig.nodeType;
	this->outFile = orig.outFile;

	this->parentId = orig.parentId;

	inverterPenetration = orig.inverterPenetration;
	softUPSPowerConsumption = orig.softUPSPowerConsumption;
	consumersCount = orig.consumersCount;
	upsLosses = orig.upsLosses;;
	upsConsumption = orig.upsLosses;
	loadSheddingCount = orig.loadSheddingCount;
	isEliteConsumer = orig.isEliteConsumer;

	//vector<Consumer>::iterator consumerIterator;
	/*Consumer c=orig.subConsumer.back();
	c.myId;*/
	for (auto consumerIterator = orig.subConsumer.begin(); consumerIterator != orig.subConsumer.end(); consumerIterator++)
	{
		subConsumer.push_back(*consumerIterator);
	}

	return *this;
}
Consumer::~Consumer(void)
{
	for_each(subConsumer.begin(), subConsumer.end(), [](Consumer *consumer){
		delete consumer;
		return true;
	});
}
void Consumer::SortSubConsumers()
{
	sort(this->subConsumer.begin(), this->subConsumer.end(),
		[](const Consumer* left, const Consumer* right){
		return left->power > right->power;
	}); //sort in descending order based on power
}
/**
* Init initializes a consumer at DISCO level.
* it initializes subconsumers like zones in a DISCO, gridstations, feeders and transformers.
*/
bool Consumer::Init(){

	TimerCallback* tcbPtr;///pointer to insert timers for each node
	EventInfo evInfo;


	if (nodeType == nt_Disco)
	{
		evInfo.eType_ = et_consumption;
		this->power = 0;
		this->upsLosses = 0;
		this->softUPSPowerConsumption = 0;;
		//		int maxZones = 10, minZones = 2;
		//int maxZones = 8, minZones = 4;
		int numberOfZones = 5;// GenerateRandom(minZones, maxZones);
		Consumer *zoneDistribution;
		this->parentId = 0;

		int currentHour = ((globalClock.globalTime) / 4347.1304) + 1;
		auto mapIterator = gCdfMap.find(currentHour);
		if (mapIterator == gCdfMap.end())
		{
			gCdfMap.clear();
			//gValData.clear();
			FileIO file;
			for (int i = currentHour; i <= currentHour + 1; i++)
			{
				gCdfMap[i] = file.ReadCdfData(i);
				sort(gCdfMap[i].begin(), gCdfMap[i].end());
				//gValData[i] = file.ReadValData(i);
			}
		}

		for (int i = 1; i <= numberOfZones; i++)
		{
			zoneDistribution = new Consumer(this->myId * 100 + i, this->outFile);
			zoneDistribution->parentId = this->myId;
			zoneDistribution->nodeType = nt_ZoneDistribution;
			ConsumerData *subConsumerData = &zoneDistribution->InitializeConsumer(&gCdfMap[currentHour]);
			FillWithConsumerData(subConsumerData);
			this->subConsumer.push_back(zoneDistribution);
			//delete zoneDistribution;
		}
		SortSubConsumers();
		//this->power/=1000;
	}
	else
	{
		evInfo.eType_ = et_idle;
	}

	evInfo.nodeId_ = myId;

	evInfo.timeLeft_ = (timeTicks)(INIT_SLOT_SYNC);

	tcbPtr = new Event(evInfo);
	gTimerManager.AddTimer(evInfo.timeLeft_, tcbPtr);

	return true;
}
/**
* InitializeConsumer initializes a consumers at different levels.
* it initializes subconsumers like zones in a DISCO, gridstations, feeders and transformers.
* based on nodeType, if it is Zone level, it will call a method to generate random grid stations.
*/
ConsumerData Consumer::InitializeConsumer(vector<double> *cdfData)
{
	switch (nodeType)
	{
	case nt_ZoneDistribution:
		InitZoneDistributions(cdfData);
		break;
	case nt_GridStation:
		InitGridStations(cdfData);
		break;
	case nt_Feederes:
		InitFeederes(cdfData);
		break;
	case nt_Transformer:
		InitTransformers(cdfData);
		break;
	default:
		OUTCON(this->myId, "InitConsumer", "Wrong consumer type initialization", 1);
		break;
	}
	//this->SortSubConsumers();
	ConsumerData data;
	data.power = this->power;
	data.losses = this->upsLosses;
	data.softUPSPower = this->softUPSPowerConsumption;
	data.consumersCount = this->consumersCount;
	data.demand = this->powerDemand;
	data.upsConsumption = this->upsConsumption;
	return data;
}
/**
* InitZoneDistributions initializes a subconsumers for zones.
* It generate random number of grid stations in given zones
*/
ConsumerData Consumer::InitZoneDistributions(vector<double> *cdfData)
{
	Consumer *consumer;
	//int maxStations = 18, minStations = 5;
	//int maxStations = 10, minStations = 5;
	int numberOfStations = 10;// GenerateRandom(minStations, maxStations);
	this->power = 0;
	this->upsLosses = 0;
	ConsumerData subConsumerData;
	for (int i = 1; i <= numberOfStations; i++)
	{
		consumer = new Consumer(this->myId * 10 + i, this->outFile);
		this->subConsumer.push_back(consumer);
		//consumer=(this->subConsumer.back());
		consumer->parentId = this->myId;
		consumer->nodeType = nt_GridStation;
		subConsumerData = consumer->InitializeConsumer(cdfData);
		FillWithConsumerData(&subConsumerData);
		/*this->subConsumer.push_back(*consumer);
		delete consumer;*/
	}
	return subConsumerData;
}
ConsumerData Consumer::InitGridStations(vector<double> *cdfData)
{
	Consumer *consumer;
	//int maxFeeders = 13, minFeeders = 8;
	int numberOfFeeders = 10;// GenerateRandom(minFeeders, maxFeeders);
	this->power = 0;
	this->upsLosses = 0;
	ConsumerData subConsumerData;
	for (int i = 1; i <= numberOfFeeders; i++)
	{
		consumer = new Consumer(this->myId * 10 + i, this->outFile);
		this->subConsumer.push_back(consumer);
		//consumer=(this->subConsumer.back());
		consumer->parentId = this->myId;
		consumer->nodeType = nt_Feederes;
		subConsumerData = consumer->InitializeConsumer(cdfData);
		FillWithConsumerData(&subConsumerData);
	}
	return subConsumerData;
}
ConsumerData Consumer::InitFeederes(vector<double> *cdfData)
{
	Consumer *consumer;
	//int maxTransformers = 30, minTransformers = 10;
	//int maxTransformers = 15, minTransformers = 10;
	int numberOfTransformers = 10;//GenerateRandom(minTransformers, maxTransformers);
	int numberOfEliteTransformers = 0;// (numberOfTransformers * GenerateRandom(0.0, 0.25));//MAX 35% off feeders as elite who consumer
													//much power;
	this->power = 0;
	this->upsLosses = 0;
	ConsumerData subConsumerData;
	for (int i = 1; i <= numberOfTransformers; i++)
	{
		consumer = new Consumer(this->myId * 10 + i, this->outFile);
		this->subConsumer.push_back(consumer);
		consumer->parentId = this->myId;
		consumer->nodeType = nt_Transformer;
		int minConsumer = 7, maxConsumer = 10;
		consumer->consumersCount = GenerateRandom(minConsumer, maxConsumer);
		//subConsumerData = consumer->InitializeConsumer(cdfData);
		if (i <= numberOfEliteTransformers)
		{
			consumer->isEliteConsumer = true;
			subConsumerData = consumer->InitTransformers(cdfData, true);
		}
		else
		{
			subConsumerData = consumer->InitializeConsumer(cdfData);
		}
		
		FillWithConsumerData(&subConsumerData);
	}
	return subConsumerData;
}
ConsumerData Consumer::InitTransformers(vector<double> *cdfData, bool isElite)
{
	this->power = 0;
	this->powerDemand = 0;
	bool shouldAddHomeConsumers = (this->subConsumer.size() < 1);
	int numberOfUpses = this->inverterPenetration*this->consumersCount;
	this->isEliteConsumer = isElite;
	double upsCons = 0;
	if (!isSoftUPS && !shouldAddHomeConsumers)
	{
		upsCons = this->upsConsumption;
	}
	this->upsLosses = 0;// ((inverterPenetration*consumersCount)*upsRating*(1 - invterEfficiency) / invterEfficiency);
	this->upsConsumption = 0;
	this->softUPSPowerConsumption = 0;
	Consumer *consumer;
	for (int i = 0; i < consumersCount; i++)
	{
			if (shouldAddHomeConsumers)
			{
				consumer = new HomeConsumer(this->myId * 10 + 1, outFile);
				consumer->isEliteConsumer = isElite;
				consumer->RandomizePower(cdfData);
				if (i < numberOfUpses)
				{
					((HomeConsumer*)consumer)->haveUps = true;
				}
				this->subConsumer.push_back(consumer);
			}
			else
			{
				this->subConsumer[i]->RandomizePower(cdfData);
			}
			this->powerDemand += this->subConsumer[i]->power;
	}
	this->power = this->powerDemand + upsCons;// +upsLosses + softUPSPowerConsumption;
	ConsumerData data;
	data.power = this->power;
	data.demand = this->powerDemand;
	data.losses = this->upsLosses;
	data.upsConsumption = this->upsConsumption;
	data.softUPSPower = this->softUPSPowerConsumption;
	data.consumersCount = this->consumersCount;
	return data;
}

/**
* RandomizePower it randomly increase or decrease the power consumption of consumer
*/
int Consumer::RandomizePower(EventInfo& eventInformation)
{
	Output dataOut;	
	int currentHour = ((globalClock.globalTime) / 4347.1304) + 1;
	auto mapIterator = gCdfMap.find(currentHour);
	if (mapIterator == gCdfMap.end())
	{
		gCdfMap.clear();
		//gValData.clear();
		FileIO file;
		for (int i = currentHour; i <= currentHour + 1; i++)
		{
			gCdfMap[i] = file.ReadCdfData(i);
			sort(gCdfMap[i].begin(), gCdfMap[i].end());
			//gValData[i] = file.ReadValData(i);
		}
	}
	std::string message;
	std::string output;
	message = "UPS-Losees at " + to_string(currentHour) + " hour";
	double upsLosses = this->GetLosses();
	dataOut.upsLosses = upsLosses;
	output = to_string(upsLosses);
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);

	message = "UPS-Consumption at " + to_string(currentHour) + " hour";
	double upsCons = this->GetUPSConsumption();
	dataOut.upsConsumption = upsCons;
	output = to_string(upsCons);
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);

	message = "SoftUPS-Consumption at " + to_string(currentHour) + " hour";
	double softUPSCons = this->GetSoftUPSConsumption();
	dataOut.softUPSConsumption = softUPSCons;
	output = to_string(softUPSCons);
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);

	this->RandomizePower(&gCdfMap[currentHour]);
	dataOut.actualPowerDemand = this->power;
	dataOut.powerSupplied = this->power;
	gOutputData.push_back(dataOut);
	this->LossesCalculation(eventInformation, currentHour);
	int nextEventTime = 4347;
	return nextEventTime;
}
/**
* ContainsConsumer checks if the given id is contained in any subconsumer
*/
bool Consumer::ContainsConsumer(const uint_32 id) const
{
	int digits = (int)floor(log10(id));
	int divisor = (int)pow(10, digits);
	int mainId = id / divisor;
	return (this->myId == mainId);
}
/**
* ContainsConsumer checks if the given id is contained in any subconsumer
*/
void Consumer::ExecuteEventToBalanceSupplyDemand(EventInfo& eventInformation, Node *nodeGeneration, Node *nodeConsumption)
{
	double powerVariationRequired = eventInformation.powerVariation;
	if (eventInformation.nodeId_ == this->myId)
	{
		TimerCallback* tcbPtr;///pointer to insert timers for each node
		EventInfo evInfo;
		evInfo.timeLeft_ = (timeTicks)(INIT_SLOT_SYNC);
		evInfo.eType_ = et_reduceConsumption;		
		this->SortSubConsumers();
		if (loadSheddingType = lt_Fair)
		{
			sort(this->subConsumer.begin(), this->subConsumer.end(),
				[](const Consumer* left, const Consumer* right){
				return (left->loadSheddingCount < right->loadSheddingCount);
			}); //sort in descending order based on power
		}
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			if (powerVariationRequired < 2)
			{
				break;
			}
			else if ((*iterator)->power >= powerVariationRequired)
			{
				evInfo.nodeId_ = (*iterator)->myId;
				evInfo.powerVariation = powerVariationRequired;
				tcbPtr = new Event(evInfo);
				gTimerManager.AddTimer(evInfo.timeLeft_, tcbPtr);
				powerVariationRequired -= evInfo.powerVariation;
			}
			else
			{
				if ((*iterator)->power >= powerVariationRequired / 2)
				{
					evInfo.nodeId_ = (*iterator)->myId;
					evInfo.powerVariation = powerVariationRequired / 2;
					tcbPtr = new Event(evInfo);
					gTimerManager.AddTimer(evInfo.timeLeft_, tcbPtr);
				}
				else
				{
					evInfo.powerVariation = (*iterator)->power;
					evInfo.nodeId_ = (*iterator)->myId;
					tcbPtr = new Event(evInfo);
					gTimerManager.AddTimer(evInfo.timeLeft_, tcbPtr);
				}
				powerVariationRequired -= evInfo.powerVariation;
			}
		}
		OUTCON(eventInformation.nodeId_, "ExecuteEventToBalanceSupplyDemand", "Requested to reduce power consumption", 3);
		OUTFILE(outFile, eventInformation.nodeId_, "ExecuteEventToBalanceSupplyDemand", "Requested to reduce power consumption", 3);
	}
	else
	{
		//vector<con>::iterator nodePosition;
		//TODO:: wouldnt it be more efficient to once sort these vectors and then simply index into the Vector using the nodeId?		
		std::string output = "Received request to reduce " + to_string(powerVariationRequired) + " power";
		OUTFILE(outFile, eventInformation.nodeId_, "GSReducingPowerConsumption", output.c_str(), 3);
		int nodeId = eventInformation.nodeId_;
		auto nodePosition = find_if(subConsumer.begin(), subConsumer.end(), [nodeId](Consumer *con){
			return *con == nodeId;
		});
		double oldPower = (*nodePosition)->power;
		output = "Current power " + to_string(this->power);
		OUTCON(eventInformation.nodeId_, "GSReducingPowerConsumption", output.c_str(), 4);
		OUTFILE(outFile, eventInformation.nodeId_, "GSReducingPowerConsumption", output.c_str(), 4);
		this->power -= oldPower;
		this->powerDemand -= (*nodePosition)->powerDemand;
		this->upsLosses -= (*nodePosition)->upsLosses;
		this->upsConsumption -= (*nodePosition)->upsConsumption;
		this->softUPSPowerConsumption -= (*nodePosition)->softUPSPowerConsumption;
		double newPower = oldPower - powerVariationRequired;
		int oldFeederCount = gFeedersTurnedOff.size();
		ConsumerData data = (*nodePosition)->ReducePower(powerVariationRequired); /** Will ask subconsumers to
																				help it reducing power supply*/
		auto lastOutputDataInstance = gOutputData.back();
		gOutputData.pop_back();
		lastOutputDataInstance.numberOfFeedersTurnedOff += (gFeedersTurnedOff.size() - oldFeederCount);
		lastOutputDataInstance.powerSupplied = this->power;
		gOutputData.push_back(lastOutputDataInstance);
		this->power += data.power;
		this->powerDemand += data.power;
		this->upsLosses += data.losses;
		this->upsConsumption += data.upsConsumption;
		this->softUPSPowerConsumption += data.softUPSPower;
		//(*nodePosition).ReducePower(Node::powerReduction);
		//SortSubConsumers();
		output = "New power " + to_string(this->power);
		OUTCON(eventInformation.nodeId_, "GSReducingPowerConsumption", output.c_str(), 4);
		OUTFILE(outFile, eventInformation.nodeId_, "GSReducingPowerConsumption", output.c_str(), 4);
		int currentHour = ((globalClock.globalTime) / 4347.1304) + 1;
		//		this->LossesCalculation(eventInformation, currentHour);
	}

}
/**
* ReducePower will reduce the power of Consumer.
* powerReduction is a negative integer tell how much to reducde power.
*/
ConsumerData Consumer::ReducePower(double powerReduction)
{
	ConsumerData data;
	double newPower = this->power - powerReduction;
	double powerNeedToReduce = powerReduction;
	//if (this->nodeType == NodeType::nt_Transformer)
	//{
	//	//this->power+=powerReduction;
	//	this->power = 0;
	//	this->upsLosses = 0;
	//	this->upsConsumption = 0;
	//	this->softUPSPowerConsumption = 0;
	//	this->powerDemand = 0;
	//	for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
	//	{
	//		//not in a good state :( rev this
	//		//double softUpsConsumptionOnTransformerLevel = ((HomeConsumer*)(*iterator))->upsRating;
	//		(*iterator)->power = 0;// softUpsConsumptionOnTransformerLevel;
	//		if (((HomeConsumer*)(*iterator))->haveUps)
	//		{
	//			(*iterator)->upsConsumption = ((HomeConsumer*)(*iterator))->upsRating + (((HomeConsumer*)(*iterator))->upsRating*((1 - ((HomeConsumer*)(*iterator))->invterEfficiency) / ((HomeConsumer*)(*iterator))->invterEfficiency));
	//			(*iterator)->upsLosses = (((HomeConsumer*)(*iterator))->upsRating*((1 - ((HomeConsumer*)(*iterator))->invterEfficiency) / ((HomeConsumer*)(*iterator))->invterEfficiency));
	//		}
	//		(*iterator)->powerDemand = (*iterator)->power;
	//		(*iterator)->softUPSPowerConsumption = 0; // softUpsConsumptionOnTransformerLevel;
	//		this->softUPSPowerConsumption += (*iterator)->softUPSPowerConsumption;// softUpsConsumptionOnTransformerLevel;
	//		this->power += (*iterator)->power;// softUpsConsumptionOnTransformerLevel;
	//		this->upsLosses += (*iterator)->upsLosses;
	//		this->upsConsumption += (*iterator)->upsLosses;
	//		this->powerDemand += (*iterator)->powerDemand;
	//	}
	//}
	if (this->nodeType == NodeType::nt_Feederes)
	{
		gFeedersTurnedOff.push_back(this->myId);
		this->ShutDownConsumer();
	}
	else
	{
		this->loadSheddingCount++;
		this->SortSubConsumers();
		if (loadSheddingType = lt_Fair)
		{
			sort(this->subConsumer.begin(), this->subConsumer.end(),
				[](const Consumer* left, const Consumer* right){
				return (left->loadSheddingCount < right->loadSheddingCount);
			}); //sort in descending order based on power
		}
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			double iteratorCurrentPower = (*iterator)->power;
			double iteratorCurrentUpsLosses = (*iterator)->upsLosses;
			double iteratorCurrentUpsConsumption = (*iterator)->upsConsumption;
			double iteratorCurrentSoftUpsConsumtion = (*iterator)->softUPSPowerConsumption;
			double iteratorPowerDemand = (*iterator)->powerDemand;
			if (powerNeedToReduce <= 1)
			{
				break;
			}
			else
			{
				this->power -= iteratorCurrentPower;
				this->upsLosses -= iteratorCurrentUpsLosses;
				this->upsConsumption -= iteratorCurrentUpsConsumption;
				this->softUPSPowerConsumption -= iteratorCurrentSoftUpsConsumtion;
				this->powerDemand -= iteratorPowerDemand;
				ConsumerData subData;
				if (iteratorCurrentPower > powerNeedToReduce)
				{
					subData = (*iterator)->ReducePower(powerNeedToReduce);
					powerNeedToReduce -= iteratorCurrentPower;
				}
				/*else if (iteratorCurrentPower > powerNeedToReduce/2)
				{
					subData = (*iterator)->ReducePower(powerNeedToReduce/2);
					powerNeedToReduce -= powerNeedToReduce / 2;
				}*/
				else
				{
					subData = (*iterator)->ShutDownConsumer();
					//powerNeedToReduce = powerNeedToReduce-iteratorCurrentPower+subData.power;
					//(*iterator).ShutDownConsumer();
					//this->power+=(*iterator).ReducePower(0,-currentPower);
				}
				this->power += subData.power;
				this->upsLosses += subData.losses;
				this->upsConsumption += subData.upsConsumption;
				this->softUPSPowerConsumption += subData.softUPSPower;
				this->powerDemand += subData.demand;
			}
		}
	}
	//rev this
	data.power = this->power;
	data.losses = this->upsLosses;
	data.upsConsumption = this->upsConsumption;
	data.softUPSPower = this->softUPSPowerConsumption;
	data.demand = this->powerDemand;
	return data;
}
/**
* ShutDownConsumer will make the power consumption all subconsumers to zero.
* Therefore the power of that consumer will become zero.
*/
ConsumerData Consumer::ShutDownConsumer()
{
	this->power = 0;
	this->powerDemand = 0;
	this->upsLosses = 0;
	this->upsConsumption = 0;
	this->softUPSPowerConsumption = 0;
	this->loadSheddingCount++;
	ConsumerData data(0, 0, 0, 0, 0, 0);
	if (this->nodeType == nt_Transformer)
	{
		/*this->softUPSPowerConsumption=consumers*upsRating;
		this->power=this->softUPSPowerConsumption;*/
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			ConsumerData retData=(*iterator)->ShutDownConsumer();
			
			data.softUPSPower += retData.softUPSPower;
			data.upsConsumption += retData.upsConsumption;
			data.losses += retData.losses;
			data.demand += retData.demand;
			data.power += retData.power;
		}
	}
	else
	{
		if (this->nodeType == NodeType::nt_Feederes)
		{
			gFeedersTurnedOff.push_back(this->myId);
		}
		for (auto consumer = subConsumer.begin(); consumer != subConsumer.end(); consumer++)
		{
			ConsumerData subConsumerData = (*consumer)->ShutDownConsumer();
			data.softUPSPower += subConsumerData.softUPSPower;
			data.power += subConsumerData.power;
			data.losses += subConsumerData.losses;
			data.upsConsumption += subConsumerData.upsConsumption;
			data.demand += subConsumerData.demand;
		}
	}
	this->power = data.power;
	this->softUPSPowerConsumption = data.softUPSPower;
	this->upsLosses = data.losses;
	this->upsConsumption = data.upsConsumption;
	this->powerDemand = data.demand;
	return data;
}
/**
* RandomizePower will randomize the power of nodes.
*/
ConsumerData Consumer::RandomizePower(vector<double> *cdfData)
{
	/*double consumerPower=this->power;
	double inverterLosses=this->upsLosses;*/
	ConsumerData subConsumerData;
	subConsumerData.losses = this->upsLosses;
	subConsumerData.upsConsumption = this->upsConsumption;
	subConsumerData.power = this->power;
	subConsumerData.softUPSPower = this->softUPSPowerConsumption;
	subConsumerData.demand = this->powerDemand;
	int numOfSubConsumer = this->subConsumer.size();
	int nodesToRandomize = numOfSubConsumer;// numOfSubConsumer > 0 ? GenerateRandom(1, numOfSubConsumer) : 0;
	vector<Consumer*>::iterator iterator = this->subConsumer.begin();
	for (int i = 0; i < nodesToRandomize; i++)
	{
		subConsumerData.power -= (*iterator)->power; //subtract last power of this consumer
		subConsumerData.losses -= (*iterator)->upsLosses; //subtract last ups loss of this consumer
		subConsumerData.upsConsumption -= (*iterator)->upsConsumption; //subtract last ups loss of this consumer
		subConsumerData.softUPSPower -= (*iterator)->softUPSPowerConsumption; /** subtract last SoftUPS consumtpion
																			of this consumer*/
		subConsumerData.demand -= (*iterator)->powerDemand; //subtract last powerdemand for this consumer
		ConsumerData data;
		if (this->nodeType == nt_Feederes)
		{
			data = (*iterator)->InitTransformers(cdfData);
		}
		else
		{
			data = (*iterator)->RandomizePower(cdfData);
		}
		subConsumerData.losses += (*iterator)->upsLosses; //add new loss of this consumer
		subConsumerData.upsConsumption += (*iterator)->upsConsumption; //add new loss of this consumer
		subConsumerData.power += (*iterator)->power; //add new  power of this consumer
		subConsumerData.softUPSPower += (*iterator)->softUPSPowerConsumption; /** add new SoftUPS consumtpion
														 of this consumer*/
		subConsumerData.demand += (*iterator)->powerDemand; //add last powerdemand for this consumer
		iterator++;
	}
	this->power = subConsumerData.power;
	this->upsLosses = subConsumerData.losses;
	this->upsConsumption = subConsumerData.upsConsumption;
	this->softUPSPowerConsumption = subConsumerData.softUPSPower;
	this->powerDemand = subConsumerData.demand;
	return subConsumerData;
}
//bool Consumer::ValidatePower()
//{
//	double thisActualPower=this->GetPower();
//	return this->power==thisActualPower;
//}
//double Consumer::GetPower()
//{
//	double sumPower=0;
//	if(this->nodeType==nt_Transformer)
//	{
//		sumPower=this->power;
//	}
//	else
//	{
//		for(auto iterator=subConsumer.begin();  iterator!=subConsumer.end(); iterator++)
//		{
//			sumPower+=(*iterator)->GetPower();
//		}
//	}
//	return sumPower;
//}
double Consumer::GetLosses()
{
	double sumPower = 0;
	if (this->nodeType == nt_Transformer)
	{
		//sumPower = this->upsLosses;
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			sumPower += (*iterator)->upsLosses;;
		}
	}
	else
	{
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			sumPower += (*iterator)->GetLosses();
		}
	}
	return sumPower;
}
double Consumer::GetUPSConsumption()
{
	double sumPower = 0;
	if (this->nodeType == nt_Transformer)
	{
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			sumPower += ((HomeConsumer*)(*iterator))->upsConsumption;;
		}
	}
	else
	{
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			sumPower += (*iterator)->GetUPSConsumption();
		}
	}
	return sumPower;
}
double Consumer::GetSoftUPSConsumption()
{
	double sumPower = 0;
	if (this->nodeType == nt_Transformer)
	{
		sumPower = this->softUPSPowerConsumption;
	}
	else
	{
		for (auto iterator = subConsumer.begin(); iterator != subConsumer.end(); iterator++)
		{
			sumPower += (*iterator)->GetSoftUPSConsumption();
		}
	}
	return sumPower;
}
void Consumer::FillWithConsumerData(ConsumerData *data)
{
	this->powerDemand += data->demand;
	this->power += data->power;
	this->upsLosses += data->losses;
	this->upsConsumption += data->upsConsumption;
	this->softUPSPowerConsumption += data->softUPSPower;
	this->consumersCount += data->consumersCount;
}
void Consumer::LossesCalculation(EventInfo& eventInformation, int currentHour)
{
	std::string message;
	std::string output;
	gTotalUpsLosses += upsLosses;
	message = "Randomize-Consumption";
	output = to_string(power);
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	message = "Consumer_losses";
	output = "Losses= " + to_string(upsLosses) + " at " + to_string(currentHour) + " hour";
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	//double actualPower = this->power - this->upsLosses - this->softUPSPowerConsumption;
	double totalUpsConsumption = this->upsLosses;//actualPower + this->upsLosses;
	double totalSoftUpsConsumption = this->softUPSPowerConsumption; //actualPower + this->softUPSPowerConsumption; //!=GetSoftUPSConsumption() 
	gTotalSoftUpsConsumption += this->softUPSPowerConsumption;
	gSaving += totalUpsConsumption - totalSoftUpsConsumption;
	message = "SoftUPS Consumption";
	output = "Consumption= " + to_string(softUPSPowerConsumption) + " at " + to_string(currentHour) + " hour";
	OUTCON(eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
	OUTFILE(outFile, eventInformation.nodeId_, message.c_str(), output.c_str(), 2);
}

/**
* NormalDistributionRandom generate a normal distribtion.
* It takes two integers mean and standard deviation
*/
int Consumer::NormalDistributionRandom(int mean, int deviation)
{
	std::random_device rd;
	std::mt19937 e2(rd());
	std::normal_distribution<> dist(mean, deviation);
	return dist(e2);
}
/**
* NormalDistributionRandom generate a normal distribtion.
* It takes two integers mean and standard deviation
*/
double Consumer::NormalDistributionRandom(double mean, double deviation)
{
	std::random_device rd;
	std::mt19937 e2(rd());
	std::normal_distribution<> dist(mean, deviation);
	return dist(e2);
}