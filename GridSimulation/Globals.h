#include <map>
cVTime globalClock;///Our global clock!
vector<Node*> gNodeVector;///vector that will keep track of all the nodes in the Simulator
//vector<nodeInfo> gNodeInfoVector;
Timers gTimerManager;
vector<double> gXiData;
std::map<int, vector<double>> gCdfMap;
vector<uint_32> gFeedersTurnedOff;
vector<Output> gOutputData;
//std::map<int, vector<double>> gValData;
double gTotalUpsLosses = 0;
double gTotalSoftUpsConsumption = 0;
double gSaving = 0;
//int Node::powerReduction=0;
//Path to you REDD data parsing files.
//string FileIO::DiretoryPath="C:/Users/Zaksh/Documents/MATLAB/All home data hourly oneday/parsedDataWeekends";
//string FileIO::DiretoryPath = "C:/Users/Zaksh/Documents/MATLAB/histogram";//"C:/Users/Zaksh/Documents/MATLAB/27-04-2011";
string FileIO::DiretoryPath = "E:/SysNet-SVN/HomeOS-Group/thesis/Zohaib/Scripts/ReadAllFiles of Red Parsed data/ksdensity";
uint_32 Node::MAX_POWER_GENERATION = 10000;
bool isSoftUPS = false;
LoadSheddingType loadSheddingType = lt_Fair;
int gHomeAgents = 0;

bool LoadConfigFile(char *file)/**< a pointer to the first character in the file name string */
{
	char *p = NULL;
	char buffer[20480];
	int fsize = 0;
	FILE *fp;
	fp = fopen(file, "rt");
	if (fp == NULL)
		return false;
	char line[10240];
	bool isComment = false;
	bool isData = false;
	bool isConfigProperlyRead = false;
	int fileCounter = 0;
	while (fgets(line, sizeof(line), fp) != NULL)
	{
		int len;
		char subst[65536];
		char *c = strstr(line, "\n");  /*Check if line contains \n*/
		if (c != NULL) /* If \n found remove it */
			strcpy(c, "");
		if (strcmp(line, "/*")==0)
			isComment = true;
		else if (isComment)
		{
			if (strcmp(line, "*/") == 0)
			{
				isComment = false;
			}
		}
		else if (line[0] == '{')
		{
			isData = true;
		}
		else if (line[0] == '}')
		{
			isData = false;
			isConfigProperlyRead = true;
			break;
		}
		else if (isData)
		{
			c = strstr(line, ",");
			if (c != NULL) /* truncate at comment */
				strcpy(c, "");
			switch (fileCounter)
			{
			case 0:
				gHomeAgents = atoi(line);
				break;
			case 1:
				FileIO::DiretoryPath = string(line);
				break;
			case 2:
				Node::MAX_POWER_GENERATION = atof(line);
				break;
			case 3:
				loadSheddingType = (LoadSheddingType)atoi(line);
				break;
			case 4:
				if (strcmp(line, "UPS") == 0)
				{
					isSoftUPS = false;
				}
				break;
			default:
				break;
			}
			fileCounter++;
		}
	}
	return isConfigProperlyRead;

}
void LoadConfigration()
{
	LoadConfigFile("Input.cfg");
}
